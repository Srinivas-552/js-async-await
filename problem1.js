const fs = require('fs');

function createFolder(folderName) {
    return new Promise((resolve, reject) => {
        fs.mkdir(folderName, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve(`${folderName} Folder created Successfully`);
            }
        })
    })
}

function createJsonFile (folderPath, numberOfFiles) {
    let newFileNamesArr = [];
    return new Promise((resolve, reject) => {
        for (let index=0; index<numberOfFiles; index++){

            let filePath = `${folderPath}/file${index+1}.json`
            newFileNamesArr.push(filePath)
            fs.writeFile(filePath, `Hello`, (err) => {
                if (err) {
                    reject(err);
                } else {
                    // console.log(`${filePath} JSON File created Successfully`)
                    resolve(newFileNamesArr);    
                }
            })
        }
    })
}

function deleteJsonFile (filePathArr) {
    return new Promise((resolve, reject) => {
        for (let index=0; index<filePathArr.length; index++){
            fs.unlink(filePathArr[index], (err) => {
                if (err) {
                    reject(err);
                } else {
                    // console.log(`${filePathArr[index]} JSON File  Deleted`);
                    resolve("All files deleted")
                }
            })
        }
    })
    
}

module.exports = {createFolder, createJsonFile, deleteJsonFile};
