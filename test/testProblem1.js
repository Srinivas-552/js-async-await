const {createFolder, createJsonFile, deleteJsonFile} = require('../problem1');

const path = require("path")

let folderPath = path.resolve(__dirname, '../JSONFiles')
// let file1 = path.resolve(__dirname, '../JSONFiles/file1.json')
// let file1 = path.resolve(__dirname, '../JSONFiles/file2.json')


const asyncAwaitFn = (async () => {
    try{
        console.log(await createFolder('JSONFiles'))
        let createdFilesdata = await createJsonFile(folderPath, 3)
        console.log(createdFilesdata);
        console.log(await deleteJsonFile(createdFilesdata))
    } catch(err) {
        console.log(err)
    }
    
})()

