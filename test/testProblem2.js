const {readFileData, convertDataToUppercase, convertToLowerCaseAndSplitSentences, readFilesAndSortContent, readFileAndDeleteContent} = require('../problem2');


const asyncAwaitFn = (async () => {
    try {
        let data = await readFileData("lipsum.txt")
        console.log("Read file data");
        console.log(await convertDataToUppercase("uppercaseData.txt", data))
        console.log(await convertToLowerCaseAndSplitSentences("uppercaseData.txt", "lowercaseData.txt"))
        await readFilesAndSortContent("lowercaseData.txt", "sortedData.txt")
        console.log(await readFileAndDeleteContent("filenames.txt"))
    } catch (err) {
        console.log(err);
    }
})()






